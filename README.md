# Formstack Bot

Utilizing the formstack.com API (http://developers.formstack.com) to submit a form using a IRC bot within a IRC channel.

##Dependencies

- "guzzlehttp/guzzle": "4.*",
- "phergie/phergie-irc-connection": "1.1.0",
- "phergie/phergie-irc-client-react": "1.0.0"

##Install

- create .env.local.php (use .env.example.php) file within root and set the database and formstack API settings within there.
- run the following commands in the terminal (in root of your project)
**php artisan key:generate**
**composer update**
**php artisan migrate**