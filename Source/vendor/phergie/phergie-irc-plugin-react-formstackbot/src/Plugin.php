<?php
/**
 * Phergie (http://phergie.org)
 *
 * @link https://github.com/dstockto/phergie-irc-plugin-react-daddy for the canonical source repository
 * @copyright Copyright (c) 2008-2014 Phergie Development Team (http://phergie.org)
 * @license http://phergie.org/license New BSD License
 * @package Phergie\Irc\Plugin\React\Daddy
 */
namespace bkmorse\Phergie\Irc\Plugin\React\FormStackBot;

use \GuzzleHttp\Client;
use Phergie\Irc\Bot\React\AbstractPlugin;
use Phergie\Irc\Bot\React\EventQueue;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Phergie\Irc\Event\Event;
use Phergie\Irc\Event\UserEvent;

/**
 * Plugin for parsing messages
 *
 * @category Phergie
 * @package bkmorse\Phergie\Irc\Plugin\React\FormStackBot
 */
class Plugin extends AbstractPlugin
{
    /**
     * Returns a mapping of events to applicable callbacks.
     *
     * @return array Associative array keyed by event name referencing strings
     *         containing names of instance methods in the class implementing
     *         this interface or valid callables
     */
    public function getSubscribedEvents()
    {
        return [
            'irc.received.privmsg' => 'handleDaddyMessage',
            'command.say'           => 'handleSayCommand',
        ];
    }

    public function handleDaddyMessage(Event $event, EventQueue $queue)
    {
/*        if ($event instanceof UserEvent) {
            $nick = $event->getNick();
            $channel = $event->getSource();
            $params = $event->getParams();
            $text = $params['text'];
            $matched = preg_match("/\s*who'?s (?:your|ya) ([^?]+)[?]?/i", $text, $matches);
            if ($matched) {
                $msg = "You're my " . $matches[1] . ', ' . $nick . '!';
                $queue->ircPrivmsg($channel, $msg);
            }
        }*/

         if ($event instanceof UserEvent) {
            $nick = $event->getNick();
            $for_example = "for example: addme Chuck, Norris, chuck@norris.com";
            $channel = $event->getSource();
            $params = $event->getParams();
            $text = trim($params['text']);
            $newsletter_signup = stripos($text, 'newsletter') !== false;
            $addme = stripos(trim($text), 'addme ') !== false;
            if ($newsletter_signup) {
                $msg = "So you want to sign up for our newsletter? Reply with addme with your first name, last name and email, separated by commas, for example: addme Chuck, Norris, chuck@norris.com";
                $queue->ircPrivmsg($channel, $msg);
            } elseif($addme) {
                if(strpos(trim($text), "addme") !== 0)
                {
                    $msg = "Please try again, addme is required at the very beginning of your message, " . $for_example;
                    $queue->ircPrivmsg($channel, $msg);

                } else {
                    $info = explode(',', str_replace("addme ", "", $text));

                    if(count($info) !== 3)
                    {
                        $msg = "You need to ONLY provide 3 pieces of information, first name, last name, email, " . $for_example . " --you entered: " . $text;
                        $queue->ircPrivmsg($channel, $msg);
                    } else {
                        // print_r($info);

                        $client = new \GuzzleHttp\Client();

                        $api_url = "https://www.formstack.com/api/v2/form/".$_ENV['FORMSTACK_FORM_ID'].".json?oauth_token=".$_ENV['FORMSTACK_TOKEN'];

                        $response = $client->get($api_url);

                        $form_info = $response->getBody();

                        $form_json = $response->json();

                        foreach($form_json["fields"] as $field):
                            print "<p>".$field["label"]." - ".$field["type"]."</p>";
                        endforeach;

                        print '<pre>';

                        print_r($form_json["fields"]);

                        $msg = "Thanks for signing up! We received your info: " . str_replace("addme ", "", $text);
                        $queue->ircPrivmsg($channel, $msg);
                    }
                }
            }
        }
    }

    public function handleSayCommand(Event $event, EventQueueInterface $queue)
    {
        if ($event instanceof UserEvent) {
            $nick = $event->getNick();
            $channel = $event->getSource();
            $params = $event->getParams();
            $text = $params['text'];
            // $matched = preg_match("/\s*who'?s (?:your|ya) ([^?]+)[?]?/i", $text, $matches);
            // if ($matched) {
                $msg = "You're my " . $matches[1] . ', ' . $nick . '!';
                $queue->ircPrivmsg($channel, $msg);
            // }
        }
    }

}
