<?php

use Phergie\Irc\Connection;

return array(

    // Plugins to include for all connections

    'plugins' => array(
        new \Phergie\Irc\Plugin\React\AutoJoin\Plugin(array(
            'channels' => '#formstackbot',
        )),
        // new \PSchwisow\Phergie\Plugin\Puppet\Plugin,

        new \Phergie\Irc\Plugin\React\MagicEightBall\Plugin(
            new \Phergie\Irc\Plugin\React\MagicEightBall\EightballProvider()
        ),

        new \bkmorse\Phergie\Irc\Plugin\React\FormStackBot\Plugin(array(

            // All configuration is optional

            'prefix' => '!', // string denoting the start of a command

            // or

            'pattern' => '/^!/', // PCRE regular expression denoting the presence of a
                                 // command

            // or

            'nick' => true, // true to match common ways of addressing the bot by its
                            // connection nick

        )),
    ),

    'connections' => array(

        new Connection(array(

            // Required settings

            'serverHostname' => 'irc.freenode.net',
            'username' => 'murman',
            'realname' => 'Murray',
            'nickname' => 'murman',

            // Optional settings

            // 'hostname' => 'user server name goes here if needed',
            // 'serverport' => 6667,
            // 'password' => 'password goes here if needed',
            // 'options' => array(
            //     'transport' => 'ssl',
            //     'force-ipv4' => true,
            // )

        )),

    )

);
