<?php

use Phergie\Irc\Plugin\React\Command\CommandEvent;
use Phergie\Irc\Bot\React\EventQueueInterface;
use Phergie\Irc\Bot\React\PluginInterface;

class FooPlugin implements PluginInterface
{
    public function getSubscribedEvents()
    {
        return array('command.foo' => 'handleFooCommand', 'command.chuck' => 'handleChuckCommand');
    }

    public function handleFooCommand(CommandEvent $event, EventQueueInterface $queue)
    {
        print "howdy from foo";
        $commandName = $event->getCustomCommand();
        $fooParams = $event->getCustomParams();
        // ...
    }

    public function handleChuckCommand()
    {
        print 'i love chuck norris';
    }
}