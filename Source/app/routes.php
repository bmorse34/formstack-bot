<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

/*
	$config = array(
    "server"       => "irc.freenode.net",
    "port"         => 6697,
    "ssl"          => true,
    "username"     => "examplebot",
    "realname"     => "example IRC Bot",
    "nick"         => "examplebot",
    "password"     => "password-for-nickserv",
    "connection_password" => "connection-password",
    "channels"     => array( '#example-channel' ),
    "unflood"      => 500,
    "admins"       => array( 'example' ),
    "debug"        => true,
    "log"          => __DIR__ . '/bot.log',
);
*/

Route::get('/pizza', function() {
	echo 'pizza!';
});

Route::get('/irc', function()
{
	$connection = new \Phergie\Irc\Connection();
	$connection->setServerHostname('irc.freenode.net');
	$connection->setServerPort(6667);
	$connection->setPassword('12345678');
	$connection->setNickname('bkmorse');
	$connection->setUsername('bkmorse');

	// echo $connection->getServerHostname();

	$client = new \Phergie\Irc\Client\React\Client();
	$client->on('irc.received', function($message, $write, $connection, $logger) {
	    if ($message['command'] !== 'JOIN') {
	        return;
	    }
	    $channel = $message['params']['channels'];
	    $nick = $message['nick'];
	    $write->ircPrivmsg($channel, 'Welcome ' . $nick . '!');
	});
	$client->run($connection);


});


Route::get('/get', function()
{
	$client = new GuzzleHttp\Client();

	$api_url = "https://www.formstack.com/api/v2/form/".$_ENV['FORMSTACK_FORM_ID'].".json?oauth_token=".$_ENV['FORMSTACK_TOKEN'];

	$response = $client->get($api_url);

	$form_info = $response->getBody();

	$form_json = $response->json();

	foreach($form_json["fields"] as $field):
		print "<p>".$field["label"]." - ".$field["type"]."</p>";
	endforeach;

	print '<pre>';

	print_r($form_json["fields"]);
});