<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class IrcConnectCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'irc-connect';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$connection = new \Phergie\Irc\Connection();
		$connection->setServerHostname('irc.freenode.net');
		$connection->setServerPort(6667);
		$connection->setPassword('12345678');
		$connection->setNickname('larrybirdman_');
		$connection->setUsername('larrybirdman_');
		$connection->setRealname('larrybirdman_');

		// echo $connection->getServerHostname();

		$client = new \Phergie\Irc\Client\React\Client();
		$client->on('irc.received', function($message, $write, $connection, $logger) {
		    if ($message['command'] !== 'JOIN') {
		        return;
		    }
		    $channel = $message['params']['formstackbot'];
		    // $channel = $message['params']['channels'];
		    $nick = $message['nick'];
		    $write->ircPrivmsg($channel, 'Welcome ' . $nick . '!');
		});
		$client->run($connection);
	}
}
